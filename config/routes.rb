ProyectoAti::Application.routes.draw do
 
  resources :tareas

  resources :usuarios

  resources :tipos

  #get "/tipos/create" to: tipos_controller:create

post 'tipos/create/:id', to: 'tipos#create'
put 'tipos/update/:id', to: 'tipos#update'
delete 'tipos/eliminar/:id', to: 'tipos#destroy'
post 'usuarios/create/:id', to: 'usuarios#create'
post 'usuarios/autentica/:id/:password', to: 'usuarios#autenticar'
post 'usuarios/olvido/:id/:secretQ/:secretA', to: 'usuarios#olvido'
put 'usuarios/edit/:id/:password', to: 'usuarios#editar'
post 'tareas/create/:id', to: 'tareas#create'
get 'tareas/listar/:id_usuario', to: 'tareas#listar'
put 'tareas/edit/:id/:id_usuario', to: 'tareas#actualizar'
delete 'tareas/eliminar/:id/:id_usuario', to: 'tareas#eliminar'
put 'tareas/agendar/:id/:id_usuario/:momento', to: 'tareas#agendar'



get ':controller(/:action(/:id(.:format)))'
post ':controller(/:action(/:id(.:format)))'
root 'login#login'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
