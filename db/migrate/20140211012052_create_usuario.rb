class CreateUsuario < ActiveRecord::Migration
  def change
    create_table :usuario do |t|
      t.int :id
      t.string :CI
      t.string :nombreU
      t.string :apellidoU
      t.string :email
      t.string :username
      t.string :password
      t.string :secretQ
      t.string :secretA

      t.timestamps
    end
  end
end
