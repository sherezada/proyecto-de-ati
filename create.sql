DROP DATABASE IF EXISTS proyecto;

CREATE DATABASE proyecto;
USE proyecto;

DROP TABLE IF EXISTS tipo;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS tarea;


CREATE TABLE tipo (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, descripcionT VARCHAR(200), id_usuario INT);

CREATE TABLE usuario (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, cedula VARCHAR(50) UNIQUE, nombreU VARCHAR(20), apellidoU VARCHAR(20), email VARCHAR(50), manana VARCHAR(20), tarde VARCHAR(20), noche VARCHAR(20), username VARCHAR(20), password VARCHAR(50), secretQ VARCHAR(500), secretA VARCHAR(500));

Create Table tarea (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, descripcion VARCHAR(200), fechaC VARCHAR(50), fechaPrim VARCHAR(50), fechaplan VARCHAR(50), prioridad VARCHAR(50), estado VARCHAR(50), id_tipoT INT, id_usuario INT, horaplan VARCHAR(20)); 

ALTER TABLE tarea ADD CONSTRAINT fk_tipo FOREIGN KEY (id_tipoT) REFERENCES tipo(id);
ALTER TABLE tarea ADD CONSTRAINT fk_usuario FOREIGN KEY (id_usuario) REFERENCES usuario(id);
