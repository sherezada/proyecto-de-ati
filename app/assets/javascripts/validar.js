    $(document).ready(function() {  
       

        $("#registrar").validate({  
            rules: {  
                nombreU:   {required: true, sololetras: true, minlength: 3},  
                apellidoU:    {required: true, sololetras: true, minlength: 3},
                cedula:   {required: false, digits: true, minlength: 6, maxlength:8},                       
                email:   {required: true,  email: true},  
                username: {required: true, minlength: 3, maxlength: 15},
                password: {required: true, minlength: 8}
            },  
            messages: {  
                password: {  
                    minlength: "Su contraseña debe tener al menos 8 caracteres",  
                }
            }  
        });

        $("#iniciar").validate({  
                rules: {  
                    usuario:   {required: true, minlength: 3, maxlength: 15},
                    contrasena:    {required: true, minlength: 3}
                },  
            messages: {  
                contrasena: {  
                    minlength: "Su contraseña debe tener al menos 8 caracteres"  
                }  
                }  
        });  

        $("#editar").validate({  
                rules: {  
                    tareaT:   {required: true, minlength: 4, sololetras: true}
                },  
            messages: {  
                tareaT: {  
                    minlength: "Su nombre de tarea debe tener al menos 4 caracteres"  
                }  
                }  
        });

       $("#crearTipo").validate({  
                rules: {  
                    descripTipo:   {required: true, minlength: 4, maxlength: 15, sololetras: true}
                },  
            messages: {  
                descripTipo: {  
                    minlength: "Su tipo debe tener al menos 4 caracteres"  
                }  
                }  
        });  
        
        $("#crearTarea").validate({  
                rules: {  
                    tarea:   {required: true, minlength: 4, maxlength: 15, sololetras: true}
                },  
            messages: {  
                tarea: {  
                    minlength: "Su tarea debe tener al menos 4 caracteres"  
                }  
                }  
        }); 
    });  