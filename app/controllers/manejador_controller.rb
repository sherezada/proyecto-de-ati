class ManejadorController < ApplicationController

	skip_before_filter :verify_authenticity_token
	layout 'encabezado'
  
  def sesion
  	@tipos = Tipo.where(id_usuario:session[:id])
  	@respuesta= JSON.parse(RestClient.get "http://localhost:3001/tareas/listar/"+session[:id].to_s+".json")
  	@fechaActual = Time.now
  	@tareaHoy = []
  	@tareaDespues=[]
  	@tareaSinFecha = []

 	@respuesta.each_with_index do |tarea,i|

 		if tarea["fechaplan"] == "" || tarea["fechaplan"] == nil
 			@tareaSinFecha.push(tarea)
 		else
 			fechas = tarea["fechaplan"].split('-')
 			puts @fechaActual.year.to_i
 			puts @fechaActual.month.to_i
 			puts @fechaActual.day.to_i
	 		if fechas[0].to_i == @fechaActual.year.to_i && fechas[1].to_i == @fechaActual.month.to_i && fechas[2].to_i == @fechaActual.day.to_i
	 			puts "soy una tarea de hoy"
	 			@tareaHoy.push(tarea)

	 		else 
	 			if fechas[0].to_i == @fechaActual.year.to_i 
	 				if  fechas[1].to_i == @fechaActual.month.to_i
	 					if fechas[2].to_i > @fechaActual.day.to_i

	 						@tareaDespues.push(tarea)
						end
					else
						if fechas[1].to_i > @fechaActual.month.to_i
							@tareaDespues.push(tarea)
						end
					end
	 			else
	 				if fechas[0].to_i > @fechaActual.year.to_i 
	 					@tareaDespues.push(tarea)
	 			  	end
	 			end

	 		end
	 	end
 	end

  end

   def crearTipo
  	@respuesta= JSON.parse(RestClient.post "http://localhost:3001/tipos/create/"+1.to_s+".json", {descripcionT: params[:descripcionT], id_usuario: session[:id]})
  	
  	if @respuesta["status"] == "Creado"
  		flash[:mensaje] = "El tipo de tarea ha sido creado"
  		redirect_to controller: :manejador, action: :sesion
  	else
  		flash[:mensaje] = "El tipo de tarea ya existe"
  		puts flash[:mensaje]
  		redirect_to controller: :manejador , action: :sesion
  	end
  	
  end

  def crearTarea
  	@respuesta= JSON.parse(RestClient.post "http://localhost:3001/tareas/create/"+1.to_s+".json", {descripcion: params[:descripcion], id_tipoT: session[:id_tipoT],prioridad:params[:prioridad], id_usuario: session[:id], horaplan:params[:horaplan], fecha:params[:fecha]})

  	if @respuesta["status"] == "Creada"
		flash[:mensaje] = "Su tarea ha sido creada"
		redirect_to controller: :manejador , action: :sesion
	else
		flash[:mensaje] = "Su tarea no ha sido creada"
		redirect_to controller: :manejador , action: :sesion
	end 

  end

  def eliminarTarea
  	puts "ESTOY EN ELIMINARs"
 	puts params[:id]
  	@respuesta= JSON.parse(RestClient.post "http://localhost:3001/tareas/eliminar/"+params[:id].to_s+"/"+session[:id].to_s+".json", {id: params[:id], id_usuario:session[:id]} )

  	if @respuesta["status"] == "Eliminada"
		flash[:mensaje] = "La tarea ha sido eliminada"
  		redirect_to controller: :manejador , action: :sesion
  	else
  		flash[:mensaje] = "La tarea no ha sido eliminada"
  		redirect_to controller: :manejador , action: :sesion
  	end
  end

  def editarTarea
  	@tipos = Tipo.where(id_usuario:session[:id])
  	@tarea= Tarea.find(params[:id]) 	
  	puts @tarea.descripcion
	@tipo = Tipo.all
	render "manejador/editartarea"
  end

  def editarT
  		puts params[:descripcion]
  		puts params[:id]
  		puts params[:iduser]
  		url= "http://localhost:3001/tareas/edit/"+params[:id].to_s+"/"+params[:iduser].to_s+".json"
  		@respuesta= JSON.parse(RestClient.post "http://localhost:3001/tareas/edit/"+params[:id].to_s+"/"+params[:iduser].to_s+".json", {descripcion: params[:descripcion], fechaplan: params[:fechaplan], prioridad: params[:prioridad], estado: params[:estado], id_tipoT: params[:tipo], horaplan: params[:horaplan]})
  		if @respuesta["status"] == "Actualizada"
      		flash[:mensaje] = "Su tarea ha sido actualizada"
      		redirect_to controller: :manejador , action: :sesion 
    	else
    		flash[:mensaje] = "Oops ocurrio un problema y su tarea no ha sido actualizada"
      		render "manejador/sesion"
    	end
  end


end
