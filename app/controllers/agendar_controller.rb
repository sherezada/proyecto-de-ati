class AgendarController < ApplicationController
  layout 'encabezado'
  def agenda
  	@tipos = Tipo.where(id_usuario:session[:id])
  	@respuesta= JSON.parse(RestClient.get "http://localhost:3001/tareas/listar/"+session[:id].to_s+".json")
  	@fechaActual = Time.now
  	@tareaVencida = []
  	@tareaSinFecha=[]
  	@tareaHoy= []

 	@respuesta.each_with_index do |tarea,i|
 		
 		if tarea["fechaplan"] == "" || tarea["fechaplan"] == nil
 		
	 		@tareaSinFecha.push(tarea)
  		else
  			fechas = tarea["fechaplan"].split('-')
  			if fechas[0].to_i == @fechaActual.year.to_i && fechas[1].to_i == @fechaActual.month.to_i && fechas[2].to_i == @fechaActual.day.to_i
	  			@tareaHoy.push(tarea)
  			else
	  			
			 		if fechas[0].to_i == @fechaActual.year.to_i 
						if  fechas[1].to_i == @fechaActual.month.to_i
		 					if fechas[2].to_i < @fechaActual.day.to_i
			 						@tareaVencida.push(tarea)
							end
						else
							if fechas[1].to_i < @fechaActual.month.to_i
								@tareaVencida.push(tarea)
							end
						end
		 			else
		 				if fechas[0].to_i < @fechaActual.year.to_i 
		 					@tareaVencida.push(tarea)
					  	end
		 			end
		 	end
  		end
  	end
  end

  	def agendarTarea
  		@tipos = Tipo.where(id_usuario:session[:id])
 		@tarea= Tarea.find(params[:id]) 	
		@tipo = Tipo.all
		render "agendar/agendartarea"
 	end

 	def agendarT
 		@respuesta= JSON.parse(RestClient.post "http://localhost:3001/tareas/agendar/"+params[:id].to_s+".json", {hoy: params[:hoy], despues: params[:despues], horaplan: params[:horaplan]})	
 		if @respuesta["status"] == "Agendada"
      		flash[:mensaje] = "Su tarea ha sido agendada"
      		redirect_to controller: :agendar , action: :agenda 
    	else
    		flash[:mensaje] = "Oops ocurrio un problema y su tarea no ha sido agendada"
      		render "agendar/agenda"
    	end
 	end
end
