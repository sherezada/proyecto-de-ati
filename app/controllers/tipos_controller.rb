class TiposController < ApplicationController
  before_action :set_tipo, only: [:show, :edit, :update, :destroy]
   skip_before_filter :verify_authenticity_token  

  # GET /tipos
  # GET /tipos.json
  def index
    @tipos = Tipo.all
    respond_to do |format|
        format.html { render action: 'index' }
        format.json { render action: 'index' }
        format.xml {render action: 'index'}
    end
  end

  # GET /tipos/1
  # GET /tipos/1.json
  def show
    respond_to do |format|
        format.html { render action: 'show' }
        format.json { render action: 'show' }
        format.xml {render action: 'show'}
    end
  end

  # GET /tipos/new
  def new
    @tipo = Tipo.new
  end

  # GET /tipos/1/edit
  def edit
  end

  # POST /tipos
  # POST /tipos.json
  def create


    @tipo = Tipo.new(tipo_params)
    
    @tipo.id = nil

    respond_to do |format|
      if @tipo.save
        format.html { redirect_to @tipo, notice: 'Tipo was successfully created.' }
        format.json { render action: 'creado' }
        format.xml {render action: 'creado'}
      else
        format.html { render action: 'new' }
        format.json { render action: 'error' }
        format.xml {render action: 'error'}
      end
    end
  end

  # PATCH/PUT /tipos/1
  # PATCH/PUT /tipos/1.json
  def update
    respond_to do |format|
      if @tipo.update(tipo_params)
        format.html { redirect_to @tipo, notice: 'Tipo was successfully updated.' }
        format.json { render action: 'actualizado' }
        format.xml {render action: 'actualizado'}
      else
        format.html { render action: 'edit' }
        format.json { render action: 'error' }
        format.xml {render action: 'error'}
      end
    end
  end

  # DELETE /tipos/1
  # DELETE /tipos/1.json
  def destroy
    @tipo.destroy
    respond_to do |format|
      format.html { redirect_to tipos_url }
      format.json { render action: 'eliminado' }
      format.xml {render action: 'eliminado'}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo
      @tipo = Tipo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipo_params
      params.require(:tipo).permit(:id, :descripcionT)
    end
end
