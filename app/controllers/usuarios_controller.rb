class UsuariosController < ApplicationController
 # before_action :set_usuario, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token

  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuarios = Usuario.all
    respond_to do |format|
          format.html { render action: 'index' }
          format.json { render action: 'index' }
          format.xml {render action: 'index'}
      end
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
        respond_to do |format|
          format.html { render action: 'show1' }
          format.json { render action: 'show1' }
          format.xml {render action: 'show1'}
      end
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
   # @usuario = Usuario.new(usuario_params)
   puts "ENTRE AL CREATE"
    @usuario = Usuario.new
    @usuario.cedula = params[:cedula]
    @usuario.nombreU = params[:nombreU]
    @usuario.apellidoU = params[:apellidoU]
    @usuario.email = params[:email]
    @usuario.manana = params[:manana]
    @usuario.tarde = params[:tarde]
    @usuario.noche = params[:noche]
    @usuario.username = params[:username]
    @usuario.password = params[:password]
    @usuario.secretQ = params[:secretQ]
    @usuario.secretA = params[:secretA]
    puts "CREE AL USUARIO"
    
    #@usuario.id = nil 
    #@usuario.manana = nil
    #@usuario.tarde = nil
    #@usuario.noche = nil

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render action: 'creado' }
        format.xml { render action: 'creado'}
      else
        format.html { render action: 'new' }
        format.json { render action: 'error1' }
        format.xml {render action: 'error'}
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url }
      format.json { head :no_content }
    end
  end

  def autenticar
    @usuario= Usuario.find(params[:id])
    @contra= @usuario.password
      if @contra==params[:password]
        respond_to do |format|
          format.html { render action: 'show' }
          format.json { render action: 'show' }
          format.xml {render action: 'acept'}
        end
    else
      respond_to do |format|
          format.html { render action: 'error' }
          format.json { render action: 'error' }
          format.xml {render action: 'discart'}
      end
    end
  end

  def olvido
    @usuario= Usuario.find(params[:id])
    @Psecreta= @usuario.secretQ
    @Rsecreta= @usuario.secretA
    if @Psecreta==params[:secretQ] and @Rsecreta==params[:secretA]
      respond_to do |format|
          format.html { render action: 'show' }
          format.json { render action: 'show' }
          format.xml {render action: 'show'}
      end
    else
      respond_to do |format|
          format.html { render action: 'error' }
          format.json { render action: 'error' }
          format.xml {render action: 'discart'}
      end
    end
  end

  def editar
    @usuario= Usuario.find(params[:id])
    @contra= @usuario.password
    if @contra==params[:password]
      respond_to do |format|
        if @usuario.update(usuario_params)
          format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
          format.json { render action: 'actualizado' }
          format.xml { render action: 'actualizado'}
        else
          format.html { render action: 'edit' }
          format.json { render action: 'error1' }
          format.xml {render action: 'error'}
        end
      end
    else
      respond_to do |format|
        format.html { render action: 'noAct' }
        format.json { render action: 'noAct' }
        format.xml {render action: 'noAct'}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

   
end
