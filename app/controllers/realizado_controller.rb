class RealizadoController < ApplicationController
  layout 'encabezado'
  def tareas
    @tipos = Tipo.where(id_usuario:session[:id])
  	@respuesta= JSON.parse(RestClient.get "http://localhost:3001/tareas/listar/"+session[:id].to_s+".json")
  	@tareasR = []
  	@respuesta.each do |tarea|
  		if tarea["estado"] == "Completada"
  			@tareasR.push(tarea)
  		end
  	end
  end
end
