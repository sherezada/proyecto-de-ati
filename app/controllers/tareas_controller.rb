class TareasController < ApplicationController
  before_action :set_tarea, only: [:show, :edit, :update, :destroy]
  protect_from_forgery with: :null_session
  ActiveRecord::Base.logger = Logger.new(STDOUT)

  # GET /tareas
  # GET /tareas.json
  def index
    @tareas = Tarea.all
    respond_to do |format|
      format.xml { render action: 'lista'}
      format.json { render action: 'index'}
      format.html {render action: 'index'}
    end
  end

  # GET /tareas/1
  # GET /tareas/1.json
  def show
    respond_to do |format|
        format.xml { render action: 'show'}
        format.json { render action: 'show'}
        format.html {render action: 'show'}
    end
  end

  # GET /tareas/new
  def new
    @tarea = Tarea.new
  end

  # GET /tareas/1/edit
  def edit
  end

  # POST /tareas
  # POST /tareas.json
  def create
    @tarea = Tarea.new(tarea_params)
    respond_to do |format|
      if @tarea.save
        format.html { redirect_to @tarea, notice: 'Tarea was successfully created.' }
        format.json { render action: 'creada' }
        format.xml {render action: 'creada'}
      else
        format.html { render action: 'new' }
        format.json { render json: 'error' }
        format.xml {render action: 'error'}
      end
    end
  end


  # PATCH/PUT /tareas/1
  # PATCH/PUT /tareas/1.json
  def update
    respond_to do |format|
      if @tarea.update(tarea_params)
        format.html { redirect_to @tarea, notice: 'Tarea was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tarea.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tareas/1
  # DELETE /tareas/1.json
  def destroy
    @tarea.destroy
    respond_to do |format|
      format.html { redirect_to tareas_url }
      format.json { head :no_content }
    end
  end

  def eliminar
    @tarea= Tarea.find(params[:id])
    @user= @tarea.id_usuario
      if @user==params[:id_usuario].to_i
        @tarea.destroy
        respond_to do |format|
          format.html { redirect_to tareas_url }
          format.json { render action: 'eliminada' }
          format.xml { render action: 'eliminada'}
        end
      else
        respond_to do |format|
          format.html { render action: 'noElim'}
          format.json { render action: 'noElim'}
          format.xml { render action: 'noElim'}
        end
      end
  end

  def listar
    @tareas=  Tarea.where(id_usuario:params[:id_usuario])
      #format.html { rendirect_to @tarea, notice: 'Se encontraron tareas.'}
    respond_to do |format|
        format.html { render action: 'index' }
        format.json { render action: 'index'}
        format.xml {render action: 'lista'}
    end
    
  end

  def actualizar
      @tarea= Tarea.find(params[:id])
      @usuario= @tarea.id_usuario
        if @tarea.id_usuario==params[:id_usuario].to_i
          respond_to do |format|
            if @tarea.update(tarea_params)
              format.html { redirect_to @tarea, notice: 'Tarea fue actualizada satisfactoriamente.' }
              format.json { render action: 'actualizada' }
              format.xml {render action: 'actualizada'}
            else
              format.html { render action: 'edit' }
              format.json { render action: 'error'}
              format.xml { render action: 'error'}
            end
          end
        else
            respond_to do |format|
              format.html { render action: 'noElim'}
              format.json { render action: 'noActualizada'}
              format.xml { render action: 'noActualizada'}
            end
        end
  end

  def agendar
    @tarea= Tarea.find(params[:id])
    @usuario=@tarea.id_usuario
    if @usuario==params[:id_usuario].to_i
      @user= Usuario.find(@usuario)
      if params[:momento]=="manana"
        @moment= @user.manana
      else
        if params[:momento]=="tarde"
          @moment= @user.tarde
        else
          if params[:momento]=="noche"
            @moment= @user.noche
          end
        end
      end
       @tarea.horaplan= @moment
       #Fechaplan hay que actualizarla a la del dia de hoy o a la q corresponda
       respond_to do |format|
          format.html { redirect_to @tarea, notice: 'Tarea fue agendada satisfactoriamente'}
          format.json { render action: 'Agendada'}
          format.xml { render action: 'Agendada'}
        end
    else
      respond_to do |format|
        format.html { render action: 'noElim'}
        format.json { render action: 'noAgendada'}
        format.xml { render action: 'noAgendada'}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tarea
      @tarea = Tarea.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tarea_params
      params.require(:tarea).permit(:id, :descripcion, :fechaC, :fechaPrim, :fechaplan, :prioridad, :estado, :id_tipoT, :id_usuario, :horaplan)
    end

end
